variable "prefix" {
  description = "The prefix used for all resources in this environment"
  default     = "prod"
}

variable "secret_key" {
  description = "Required for Flask"
  sensitive   = true
}

variable "location" {
  description = "The Azure location to create resources in"
  default     = "UK South"
}