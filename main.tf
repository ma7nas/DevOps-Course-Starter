terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = ">= 3.8"
    }
  }

  backend "azurerm" {
    resource_group_name  = "Cohort29_NeiSte_ProjectExercise"
    storage_account_name = "nstaskmasterstorage"
    container_name       = "nstaskmastercontainer"
    key                  = "terraform.tfstate"
  }
}

provider "azurerm" {
  features {}
}

data "azurerm_resource_group" "main" {
  name = "Cohort29_NeiSte_ProjectExercise"
}

resource "azurerm_service_plan" "main" {
  name                = "terraformed-asp"
  location            = data.azurerm_resource_group.main.location
  resource_group_name = data.azurerm_resource_group.main.name
  os_type             = "Linux"
  sku_name            = "B1"
}

resource "azurerm_linux_web_app" "main" {
  name                = "terraformed-taskmastertodo"
  location            = data.azurerm_resource_group.main.location
  resource_group_name = data.azurerm_resource_group.main.name
  service_plan_id     = azurerm_service_plan.main.id

  site_config {
    application_stack {
      docker_image_name   = "ma7nas/todo-app:prod"
      docker_registry_url = "https://index.docker.io"
    }
  }

  app_settings = {
    "MONGODB_CONNECTION_STRING" = azurerm_cosmosdb_account.main.connection_strings[0]
    "MONGODB_DATABASE_NAME"     = resource.azurerm_cosmosdb_mongo_database.main.name
    "SECRET_KEY"                = "${var.secret_key}"
  }
}

resource "azurerm_cosmosdb_account" "main" {
  name                = "ns-cosmosdb-account-tf"
  resource_group_name = data.azurerm_resource_group.main.name
  location            = data.azurerm_resource_group.main.location
  offer_type          = "Standard"
  kind                = "MongoDB"

  capabilities {
    name = "EnableMongo"
  }

  capabilities {
    name = "EnableServerless"
  }

  consistency_policy {
    consistency_level       = "BoundedStaleness"
    max_interval_in_seconds = 300
    max_staleness_prefix    = 100000
  }

  geo_location {
    location          = var.location
    failover_priority = 0
  }
}

resource "azurerm_cosmosdb_mongo_database" "main" {
  name                = "taskmasterdb-tf"
  resource_group_name = data.azurerm_resource_group.main.name
  account_name        = resource.azurerm_cosmosdb_account.main.name
  lifecycle {
    prevent_destroy = true
  }
}