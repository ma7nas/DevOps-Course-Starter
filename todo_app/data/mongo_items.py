import pymongo
from todo_app.data.Item import Item
from todo_app.data.mongo_static_data_functions import myMongoDBConnection, myMongoDBName

def get_items():
    """
    Fetches all saved items from MongoDB and instantiates an Item for each one.

    Returns:
        list: The list of saved items.
    """
    client = pymongo.MongoClient(myMongoDBConnection())
    database = client[myMongoDBName()]
    todo_items = database.todo_items
    items = []
    for todo_item in todo_items.find():
        item = Item.from_mongodb(todo_item)
        items.append(item)

    return items

def add_item(name):
    """
    Adds a new item by inserting it into the todo_items collection in MongoDB

    Args:
        name: The name of the item.

    Returns:
        item: The result of the insert.
    """
    client = pymongo.MongoClient(myMongoDBConnection())
    database = client[myMongoDBName()]
    todo_items = database['todo_items']
    result = todo_items.insert_one({'name': name, 'status': 'To Do'})

    return result

def update_as_done(item_id):
    """
    Updates an existing item in MongoDB to Done. If no existing item matches the ID of the specified item, nothing is changed.

    Args:
        item_id: The id of the item to update to Done.
    """
    client = pymongo.MongoClient(myMongoDBConnection())
    database = client[myMongoDBName()]
    todo_items = database['todo_items']
    result = todo_items.update_one({'_id': item_id}, {'$set': {'status': 'Done'}})

    return result

def delete_item(item_id):
    """
    Deletes an existing item in MongoDB. If no existing item matches the ID of the specified item, nothing is deleted.

    Args:
        item_id: The id of the item to delete.
    """
    client = pymongo.MongoClient(myMongoDBConnection())
    database = client[myMongoDBName()]
    todo_items = database['todo_items']
    result = todo_items.delete_one({'_id': item_id})

    return result