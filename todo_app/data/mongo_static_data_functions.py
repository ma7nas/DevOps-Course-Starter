import os

def myMongoDBConnection():
    return os.getenv('MONGODB_CONNECTION_STRING')

def myMongoDBName():
    return os.getenv('MONGODB_DATABASE_NAME')