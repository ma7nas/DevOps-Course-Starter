import pytest
import requests
import os
import pymongo
import mongomock
from dotenv import load_dotenv, find_dotenv
from todo_app import app

@pytest.fixture
def client():
    # Use our test integration config instead of the 'real' version
    file_path = find_dotenv('.env.test')
    load_dotenv(file_path, override=True)

    # Use mongomock to create a test_client that can be used in our tests.
    with mongomock.patch(servers=((os.getenv('MONGODB_CONNECTION_STRING'), 27017),)):
        mongo_client = pymongo.MongoClient(os.getenv('MONGODB_CONNECTION_STRING'))
        database = mongo_client[os.getenv('MONGODB_DATABASE_NAME')]
        todo_items = database.todo_items
        test_todos = [ 
            {'name': 'first item', 'status': 'To Do'},
            {'name': 'second item', 'status': 'To Do'},
            {'name': 'third item', 'status': 'Done'} 
        ]
        result = todo_items.insert_many(test_todos)
        test_app = app.create_app()
        with test_app.test_client() as client:
            yield client

def test_index_page(client):

    # Make a request to our app's index page
    response = client.get('/')
    # Check the response code and the data in the response
    assert response.status_code == 200
    assert 'first item' in response.data.decode()
    assert 'third item' in response.data.decode()

def test_add_end_point(client):

    # Make a request to add a to do item
    payload = {'name': 'fourth item'}
    
    post_response = client.post('/add', data=payload)

    # Perform the redirect here ourselves
    get_response = client.get('/')

    # Check the post response performs a redirect (302), the get response code (200) and the newly added data
    assert post_response.status_code == 302
    assert get_response.status_code == 200
    assert 'fourth item' in get_response.data.decode()